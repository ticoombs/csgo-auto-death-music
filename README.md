# CSGO auto Play-Pause while you are dead

I got annoyed by having to play/pause my music every time the round started or ended. 
So I whipped up this

Enjoy

## Requirements
 - playerctl
 - python3
 - GSI
 - Linux?

## Install instructions
- Setup GSI using the cfg in repo and pop it in your `steamapps/common/csgo/cfg/`
- Modify `csgo-auto-death-music.py` with your steam_id
- install Flask `pip install --user -r requirements.txt`
- run `csgo-auto-death-music.py`
