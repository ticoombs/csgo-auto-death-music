#!/usr/bin/env python3
from flask import Flask
from flask import request
from subprocess import run
application = Flask(__name__)
debug = False
STEAMID = '76543219876543219'  # Change to your ID
STEAM_NAME = ''  # Or include your ingame CSGO name


def play_pause(action):
    # do subprocess call to playerctl
    if action == "play":
        args = "playerctl play".split()
    if action == "pause":
        args = "playerctl pause".split()
    run(args)
    print(f"RUNNING: {args}")


@application.route('/', methods=['POST'])
def index():
    if request.method == 'POST':
        data = request.get_json()
        if debug:
            print(data)

        if data["player"]["activity"] != "playing":
            # CBF if we are not "playing"
            return "200"

        try:
            if data["action"]["round_end"] == "yes":
                play_pause("play")
                print("Action-Roundend-yes: Playing")
        except Exception as e:
            print(f"error: {e}")

        try:
            if data["round"]["phase"] == "live":
                # The game is live. So we can still have music in warmup
                if data["player"]["steamid"] != STEAMID:
                    play_pause("play")
                    print("We are watching someone else, playing")
                    return "200"
                elif data["player"]["state"]["health"] < 1:
                    play_pause("play")
                    print("You Died.. Playing")
                    return "200"
                else:
                    # Your somehow not dead...
                    play_pause("pause")
                    print("Round-Phase-Over: Pausing")
                    return "200"
            if data["round"]["phase"] == "over":
                play_pause("play")
                print("Round-Phase-Over: Playing")
                return "200"
            # The round ended, check if it was from Kills or Bomb
        except Exception as e:
            print(f"error: {e}")

        # Always return OK
        return "200"


if __name__ == "__main__":
    # Make sure this port matches your gamestate configuration
    application.run(host='0.0.0.0', port=8298)
